package Config

import (
	"gin-mvc-demo/Constant"
	"gin-mvc-demo/Struct"
)

func GetDatabaseProduct() (m map[string]string) {
	m = make(map[string]string)
	m[Constant.DEFAULT_VALUE] = new(Struct.MysqlConfig).SetHost("127.0.0.1").SetPort(3306).SetUsername("root").SetPassword("1234").SetDatabase("test").SetCharset("utf8mb4").SetLoc("Local").Get()
	return
}
