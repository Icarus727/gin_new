package Config

import (
	"gin-mvc-demo/Constant"
	"strings"
)

func GetEnvList() (list []string) {
	list = []string{Constant.ENV_LOCAL, Constant.ENV_PRODUCT}
	return
}

func GetEnvString(sep string) (result string) {
	result = strings.Join(GetEnvList(), sep)
	return
}
