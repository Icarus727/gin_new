package Model

import (
    "github.com/jinzhu/gorm"
)

type Base struct {
    gorm.Model
    config string `gorm:"-"`
    table  string `gorm:"-"`
}

func (that *Base) SetConfig(config string) {
    that.config = config
}

func (that *Base) GetConfig() string {
    return that.config
}

func (that *Base) SetTable(table string) {
    that.table = table
}

func (that *Base) GetTable() string {
    return that.table
}

func (that *Base) TableName() string {
    return that.GetTable()
}
