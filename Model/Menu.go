package Model

import "gin-mvc-demo/Constant"

type Menu struct {
	Base
}

func GetMenu() *Menu {
	that := new(Menu)
	that.SetConfig(Constant.DEFAULT_VALUE)
	that.SetTable("dry_menu")
	return that
}
