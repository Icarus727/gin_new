package Service

import (
	"gin-mvc-demo/Database"
)

type Menu struct {
}

func GetMenu() *Menu {
	that := new(Menu)
	return that
}

func (*Menu) One(id string) map[string]string {
	databaseMenu := Database.GetMenu()
	return databaseMenu.One(id)
}
