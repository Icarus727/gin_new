package Database

import (
	"gin-mvc-demo/Config"
	"gin-mvc-demo/Core"
	"gin-mvc-demo/Variable"
	"github.com/jinzhu/gorm"
	"log"
)

type Base struct {
	config string
	table  string
	DB     *gorm.DB
}

func (that *Base) Set(config, table string) {
	that.config = config
	that.table = table
	m := Config.GetDatabase(Variable.Env)
	dns := m[config]
	db, err := Core.GetDatabase(config, dns)
	if err != nil {
		log.Println(err)
	}
	that.DB = db
}

func (that *Base) GetDB() *gorm.DB {
	return that.DB.Table(that.table)
}
