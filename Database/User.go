package Database

import (
	"gin-mvc-demo/Model"
)

type User struct {
	Base
}

func GetUser() *User {
	that := new(User)
	model := that.GetModel()
	that.Set(model.GetConfig(), model.GetTable())
	return that
}

func (that *User) GetModel() *Model.User {
	return Model.GetUser()
}
