package Function

import (
	"gin-mvc-demo/Constant"
	"time"
)

func GetDateTime() string {
	return time.Now().Format(Constant.DATE_TIME_FORMAT_STD)
}
