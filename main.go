package main

import (
    "Dry/Config"
    "Dry/Constant"
    "Dry/Frame"
    "Dry/Modules/Admin/Controller"
    "Dry/Modules/Api/Controller"
    "Dry/Variable"
    "flag"
    "github.com/gin-gonic/gin"
    "log"
)

var (
    env string
)

func init() {
    flag.StringVar(&env, "env", Config.GetEnvList()[0], "main -env "+Config.GetEnvString("|"))
}

func AddModuleRouter(r *gin.Engine) {
    ApiController.RegisterRouter()
    AdminController.RegisterRouter()
    Frame.RegisterRouter(r)
}

func main() {
    flag.Parse()
    Variable.Env = env
    if env == Constant.ENV_LOCAL {
        gin.SetMode(gin.DebugMode)
    } else {
        gin.SetMode(gin.ReleaseMode)
    }
    r := gin.Default()
    AddModuleRouter(r)
    err := r.Run(Variable.Port)
    if err != nil {
        log.Println(err)
    }
}
