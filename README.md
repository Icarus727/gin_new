### 配置

配置文件在`Config`目录下，实现了环境分离，目前只写了数据库部分的，需要配置其他的照着写就好了。

### 运行

1.开发调试

```
go run . -env Local(默认值)
go run . -env Product
```

环境值的不同，读取的配置文件也不同。

2.正式环境

```
go build
./Dry -env Local(默认值)
./Dry -env Product
```

### 特点

1.基于GIN。

2.利用反射实现了路由的自动注册，免得一个一个地写。

3.符合传统MVC架构的特点。

4.环境分离。

5.没有引入ORM，而是提供一个拼接SQL的库，使用简单。

6.支持数据库的单实例。

### 其他

1.这只是花了1天时间搭建的简单框架，不适合用于生产环境，但值得学习或者参考。

2.本人的另一个开源项目，基于swoole的开发框架，https://gitee.com/drycms/drycms

3.本人个人网站https://www.zhouchun.net